import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import LoadingPage from './components/LoadingPage';
import AppRouter from './AppRouter';
import configureStore from './configureStore';
import { firestore } from './firebase';
import { setFeedingRecords } from './actions/feeding';

import 'normalize.css/normalize.css';
import './styles/styles.scss';
import 'react-dates/initialize';

import * as serviceWorker from './serviceWorker';
import moment from 'moment';


const store = configureStore();

// Setup a listener - not sure the best place for this.
firestore.collection('feedingRecords').where("createdAt", ">=", moment().subtract(10, 'days').valueOf())
  .onSnapshot((querySnapshot) => {
    const feedingRecords = [];
    querySnapshot.forEach((doc) => {
      //console.log('snapshot forEach', doc.id);
      feedingRecords.push({ id: doc.id, ...doc.data() });
    });
    console.log('snapshot triggered - processing:', feedingRecords.length);
    store.dispatch(setFeedingRecords(feedingRecords));
    renderApp();
  });

// ReactDOM.render(<App />, document.getElementById('root'));

let hasRendered = false;
const renderApp = () => {
  if (!hasRendered) {
    ReactDOM.render(jsx, document.getElementById('root'));
    hasRendered = true;
  }
};

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

ReactDOM.render(<LoadingPage />, document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
