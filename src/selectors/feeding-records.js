

export const prepFeedingRecords = (feedingRecords) => {
  return feedingRecords.slice().sort((a, b) => {
    return a.createdAt < b.createdAt ? 1 : -1;
  });
}