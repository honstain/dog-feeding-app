import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import feedingReducer from './reducers/feeding';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const configureStore = () => {
  const store = createStore(
    combineReducers({
      feedings: feedingReducer,
    }),
    composeEnhancers(applyMiddleware(thunk)));
  return store;
};

export default configureStore;