import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from './components/Header';
import FeedingDashboardPage from './components/FeedingDashboardPage';
import CreateFeedingPage from './components/CreateFeedingPage';
import EditFeedingPage from './components/EditFeedingPage';


const AppRounter = () => (
  <BrowserRouter>
    <div>
      <Header />
      <Switch>
        <Route path="/" component={FeedingDashboardPage} exact={true}/>
        <Route path="/create" component={CreateFeedingPage} exact={true}/>
        <Route path="/edit/:id" component={EditFeedingPage} exact={true}/>
      </Switch>
    </div>
  </BrowserRouter>
);

export default AppRounter;