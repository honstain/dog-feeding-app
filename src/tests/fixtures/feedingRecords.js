import moment from 'moment';

const feedingRecords = [
  { id: '1', wetFoodAmount: 13000, dryFoodAmount: 4000, createdAt: moment(0).valueOf() },
  { id: '2', wetFoodAmount: 28000, dryFoodAmount: 8000, createdAt: moment(0).add(1, 'hours').valueOf() },
];

export const feedingRecordsDateTesting = [
  {
    id: '0-today',
    wetFoodAmount: 30000,
    dryFoodAmount: 3000,
    createdAt: moment('2019-06-06T06:00:00.000-0700').valueOf()
  },
  {
    id: '1-today',
    wetFoodAmount: 20000,
    dryFoodAmount: 2000,
    createdAt: moment('2019-06-06T00:00:00.000-0700').valueOf()
  },
  {
    id: '2--06/05',
    wetFoodAmount: 10000,
    dryFoodAmount: 1000,
    createdAt: moment('2019-06-05T23:30:00.000-0700').valueOf()
  },
  {
    id: '3--06/04',
    wetFoodAmount: 1000,
    dryFoodAmount: 1000,
    createdAt: moment('2019-06-04T02:00:00.000-0700').valueOf()
  },
  {
    id: '4--06/04',
    wetFoodAmount: 1000,
    dryFoodAmount: 1000,
    createdAt: moment('2019-06-04T01:00:00.000-0700').valueOf()
  },
  {
    id: '5--06/04',
    wetFoodAmount: 1000,
    dryFoodAmount: 1000,
    createdAt: moment('2019-06-04T00:00:00.000-0700').valueOf()
  },
  {
    id: '6--06/02',
    wetFoodAmount: 1000,
    dryFoodAmount: 1000,
    createdAt: moment('2019-06-02T00:00:00.000-0700').valueOf()
  },
];

export default feedingRecords;