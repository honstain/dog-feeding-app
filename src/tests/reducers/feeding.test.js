import feedingReducer from '../../reducers/feeding';
import feedingRecords from '../fixtures/feedingRecords';


test('should set default state', () => {
  const state = feedingReducer(undefined, { type: '@@INIT' });
  expect(state).toEqual([]);
});

test('should add feeding', () => {
  const feeding = feedingRecords[0];
  const state = feedingReducer([], { type: 'ADD_FEEDING' , feeding });
  expect(state).toEqual([feeding]);
});

test('should set the feeding records', () => {
  const state = feedingReducer([], { type: 'SET_FEEDING_RECORDS' , feedingRecords });
  expect(state).toEqual(feedingRecords);
});
