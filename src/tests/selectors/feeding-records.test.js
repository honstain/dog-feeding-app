import { prepFeedingRecords } from '../../selectors/feeding-records';
import feedingRecords from '../fixtures/feedingRecords';


test('should sort by date', () => {
  const result = prepFeedingRecords(feedingRecords);
  expect(result).toEqual([feedingRecords[1], feedingRecords[0]]);
});

test('should sort by date with a shallow copy', () => {
  const input = [feedingRecords[0], feedingRecords[1]];
  prepFeedingRecords(feedingRecords);
  expect(input).toEqual([feedingRecords[0], feedingRecords[1]]);
});