import React from 'react';
import { shallow } from 'enzyme';
import { FeedingList } from '../../components/FeedingList';
import feedingRecords from '../fixtures/feedingRecords';

test('should render empty feeding list correctly', () => {
  const wrapper = shallow(<FeedingList feedingRecords={[]}/>);
  expect(wrapper).toMatchSnapshot();
});

test('should render feeding list correctly with data', () => {
  const wrapper = shallow(<FeedingList feedingRecords={feedingRecords}/>);
  expect(wrapper).toMatchSnapshot();
});