import React from 'react';
import { shallow } from 'enzyme';
import moment from 'moment';
import MockDate from 'mockdate';
import { CreateFeedingPage } from '../../components/CreateFeedingPage';

let addFeedingAndStoreSpy, historySpy, wrapper;

beforeEach(() => {
  historySpy = { push: jest.fn() };
  addFeedingAndStoreSpy = jest.fn();

  MockDate.set('2019-06-06T00:00:00.000-0700');

  wrapper = shallow(<CreateFeedingPage
    history={historySpy}
    addFeedingAndStore={addFeedingAndStoreSpy}
  />);
});

afterEach(() => {
  MockDate.reset();
});

test('should render add create feeding page correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

test('should handle onSubmit', () => {
  const feeding = {
    wetFoodAmount: 4000,
    dryFoodAmount: 1000,
    createdAt: moment('2019-05-05T00:00:00.000-0700').valueOf(),
  };

  wrapper.find('FeedingForm').prop('onSubmit')(feeding);
  expect(historySpy.push).toHaveBeenCalledWith('/');
  expect(addFeedingAndStoreSpy).toHaveBeenLastCalledWith(feeding);
});
