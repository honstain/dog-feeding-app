import React from 'react';
import { shallow } from 'enzyme';
import moment from 'moment';
import MockDate from 'mockdate';

import {
  groupByDay,
  calculateLastFiveDays,
  FeedingSummary,
} from '../../components/FeedingSummary';
import feedingRecords, { feedingRecordsDateTesting } from '../fixtures/feedingRecords';


afterEach(() => {
  MockDate.reset();
});

test('should render empty FeedingSummary correctly', () => {
  MockDate.set('2019-06-07T00:00:00.000-0700');
  const wrapper = shallow(<FeedingSummary feedingRecords={[]}/>);
  expect(wrapper).toMatchSnapshot();
});

test('should render FeedingSummary correctly', () => {
  MockDate.set('2019-06-06T00:00:00.000-0700');

  const wrapper = shallow(<FeedingSummary feedingRecords={feedingRecordsDateTesting}/>);
  expect(wrapper).toMatchSnapshot();
});

test('groupByDay should return an empty object for empty input', () => {
  expect(groupByDay([])).toEqual({});
});

test('groupByDay should group by the day and summarize the food totals', () => {
  const expected = {
    '1559804400000': { day: moment('2019-06-06T00:00:00.000-0700').valueOf(), wetFood: 50000, dryFood: 5000 },
    '1559718000000': { day: moment('2019-06-05T00:00:00.000-0700').valueOf(), wetFood: 10000, dryFood: 1000 },
    '1559631600000': { day: moment('2019-06-04T00:00:00.000-0700').valueOf(), wetFood: 3000, dryFood: 3000 },
    '1559458800000': { day: moment('2019-06-02T00:00:00.000-0700').valueOf(), wetFood: 1000, dryFood: 1000 },
  };

  const results = groupByDay(feedingRecordsDateTesting);
  expect(results).toEqual(expected);
});

test('calculateLastFiveDays and groupByDay should stub days with no data', () => {
  MockDate.set('2019-06-07T00:00:00.000-0700');

  const expected = [
    { day: moment('2019-06-07T00:00:00.000-0700').valueOf(), wetFood: 0, dryFood: 0 },
    { day: moment('2019-06-06T00:00:00.000-0700').valueOf(), wetFood: 50000, dryFood: 5000 },
    { day: moment('2019-06-05T00:00:00.000-0700').valueOf(), wetFood: 10000, dryFood: 1000 },
    { day: moment('2019-06-04T00:00:00.000-0700').valueOf(), wetFood: 3000, dryFood: 3000 },
    { day: moment('2019-06-03T00:00:00.000-0700').valueOf(), wetFood: 0, dryFood: 0 },
  ];
  const groupedData = groupByDay(feedingRecordsDateTesting);
  const results = calculateLastFiveDays(groupedData);

  expect(results).toEqual(expected);
});


test('calculateLastFiveDays should stub days with no data', () => {
  MockDate.set('2019-06-07T00:00:00.000-0700');

  const input = {
    '1559804400000': { day: moment('2019-06-06T00:00:00.000-0700').valueOf(), wetFood: 50000, dryFood: 5000 },
    '1559718000000': { day: moment('2019-06-05T00:00:00.000-0700').valueOf(), wetFood: 10000, dryFood: 1000 },
  };

  const expected = [
    { day: moment('2019-06-07T00:00:00-07:00').valueOf(), wetFood: 0, dryFood: 0 },
    { day: moment('2019-06-06T00:00:00-07:00').valueOf(), wetFood: 50000, dryFood: 5000 },
    { day: moment('2019-06-05T00:00:00-07:00').valueOf(), wetFood: 10000, dryFood: 1000 },
    { day: moment('2019-06-04T00:00:00-07:00').valueOf(), wetFood: 0, dryFood: 0 },
    { day: moment('2019-06-03T00:00:00-07:00').valueOf(), wetFood: 0, dryFood: 0 },
  ];
  const results = calculateLastFiveDays(input);
  expect(results).toEqual(expected);
});

test('calculateLastFiveDays ignores days before the current datetime', () => {
  MockDate.set('2019-06-07T00:00:00.000-0700');

  const input = {
    '1559977200000': { day: moment('2019-06-08T00:00:00.000-0700').valueOf(), wetFood: 50000, dryFood: 5000 },
    '1559718000000': { day: moment('2019-06-05T00:00:00.000-0700').valueOf(), wetFood: 10000, dryFood: 1000 },
  };

  const expected = [
    { day: moment('2019-06-07T00:00:00-07:00').valueOf(), wetFood: 0, dryFood: 0 },
    { day: moment('2019-06-06T00:00:00-07:00').valueOf(), wetFood: 0, dryFood: 0 },
    { day: moment('2019-06-05T00:00:00-07:00').valueOf(), wetFood: 10000, dryFood: 1000 },
    { day: moment('2019-06-04T00:00:00-07:00').valueOf(), wetFood: 0, dryFood: 0 },
    { day: moment('2019-06-03T00:00:00-07:00').valueOf(), wetFood: 0, dryFood: 0 },
  ];
  const results = calculateLastFiveDays(input);
  expect(results).toEqual(expected);
});