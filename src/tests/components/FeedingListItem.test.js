import React from 'react';
import { shallow } from 'enzyme';
import { FeedingListItem } from '../../components/FeedingListItem';
import feedingRecords from '../fixtures/feedingRecords';

test('should render feeding list item correctly', () => {
  const wrapper = shallow(<FeedingListItem {...feedingRecords[0]} />);
  expect(wrapper).toMatchSnapshot();
});
