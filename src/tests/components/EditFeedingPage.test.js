import React from 'react';
import { shallow } from 'enzyme';
import moment from 'moment';
import MockDate from 'mockdate';
import { EditFeedingPage } from '../../components/EditFeedingPage';

let editFeedingAndStoreSpy, historySpy, deleteFeedingAndStoreSpy, wrapper;

const feeding = {
  id: 'A00000001',
  wetFoodAmount: 4000,
  dryFoodAmount: 1000,
  createdAt: moment('2019-05-05T00:00:00.000-0700').valueOf(),
};

beforeEach(() => {
  historySpy = { push: jest.fn() };
  editFeedingAndStoreSpy = jest.fn();
  deleteFeedingAndStoreSpy = jest.fn();

  MockDate.set('2019-06-06T00:00:00.000-0700');

  wrapper = shallow(<EditFeedingPage
    feeding={feeding}
    history={historySpy}
    editFeedingAndStore={editFeedingAndStoreSpy}
    deleteFeedingAndStore={deleteFeedingAndStoreSpy}
  />);
});

afterEach(() => {
  MockDate.reset();
});

test('should render edit feeding page correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

test('should handle onSubmit', () => {
  wrapper.find('FeedingForm').prop('onSubmit')(feeding);
  expect(historySpy.push).toHaveBeenCalledWith('/');
  expect(editFeedingAndStoreSpy).toHaveBeenLastCalledWith(feeding.id, feeding);
});

test('should handle onRemove', () => {
  wrapper.find('FeedingForm').prop('onRemove')(feeding.id);
  expect(historySpy.push).toHaveBeenCalledWith('/');
  expect(deleteFeedingAndStoreSpy).toHaveBeenLastCalledWith(feeding.id);
});