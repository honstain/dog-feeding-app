import React from 'react';
import { shallow } from 'enzyme';
import { FeedingDashboardPage } from '../../components/FeedingDashboardPage';
import feedingRecords from '../fixtures/feedingRecords';

test('should render empty feeding dashboard page correctly', () => {
  const wrapper = shallow(<FeedingDashboardPage feedingRecords={[]}/>);
  expect(wrapper).toMatchSnapshot();
});

test('should render feeding dashboard page correctly', () => {
  const wrapper = shallow(<FeedingDashboardPage feedingRecords={feedingRecords}/>);
  expect(wrapper).toMatchSnapshot();
});

