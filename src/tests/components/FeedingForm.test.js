import React from 'react';
import { shallow } from 'enzyme';
import moment from 'moment';
import MockDate from 'mockdate';
import { FeedingForm } from '../../components/FeedingForm';

const WET_FOOD_AMOUNT_GRAMS = '12';
const DRY_FOOD_AMOUNT_GRAMS = '7';

let onSubmitSpy, wrapper;

beforeEach(() => {
  const feeding = {
    wetFoodAmount: 4000,
    dryFoodAmount: 1000,
    createdAt: moment('2019-05-05T00:00:00.000-0700').valueOf(),
  };
  onSubmitSpy = jest.fn();

  MockDate.set('2019-06-06T00:00:00.000-0700');

  wrapper = shallow(<FeedingForm
    feeding={feeding}
    onSubmit={onSubmitSpy}
  />);
});

afterEach(() => {
  MockDate.reset();
});

test('should render feeding form correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

test('should update state on a wetFoodAmount onChange', () => {
  wrapper.find('input').at(0).simulate('change', { target: { value: WET_FOOD_AMOUNT_GRAMS }});
  wrapper.update();
  expect(wrapper.find('input').at(0).prop('value')).toBe(WET_FOOD_AMOUNT_GRAMS);
});

test('should update state on a dryFoodAmount onChange', () => {
  wrapper.find('input').at(1).simulate('change', { target: { value: DRY_FOOD_AMOUNT_GRAMS }});
  wrapper.update();
  expect(wrapper.find('input').at(1).prop('value')).toEqual(DRY_FOOD_AMOUNT_GRAMS);
});

test('should call onSubmit prop on valid form submit and set default values', () => {
  wrapper.find('input').at(0).simulate('change', { target: { value: null }});
  wrapper.find('input').at(1).simulate('change', { target: { value: null }});
  wrapper.find('form').simulate('submit', { preventDefault: () => {} });

  expect(onSubmitSpy).toHaveBeenLastCalledWith({
    wetFoodAmount: 0,
    dryFoodAmount: 0,
    createdAt: expect.any(Number),
  });
});

test('should call onSubmit prop on valid form submit', () => {
  wrapper.find('input').at(0).simulate('change', { target: { value: WET_FOOD_AMOUNT_GRAMS }});
  wrapper.find('input').at(1).simulate('change', { target: { value: DRY_FOOD_AMOUNT_GRAMS }});
  wrapper.find('form').simulate('submit', { preventDefault: () => {} });

  expect(onSubmitSpy).toHaveBeenLastCalledWith({
    wetFoodAmount: WET_FOOD_AMOUNT_GRAMS * 1000,
    dryFoodAmount: DRY_FOOD_AMOUNT_GRAMS * 1000,
    createdAt: expect.any(Number),
  });
});
