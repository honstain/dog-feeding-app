import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
  addFeeding,
  addFeedingAndStore,
  setFeedingRecords,
} from '../../actions/feeding';
import { firestore } from '../../firebase';

import feedingRecords from '../fixtures/feedingRecords';


const createMockStore = configureMockStore([thunk]);


beforeEach((done) => {
  const batch = firestore.batch();

  // WARNING - this grabs EVERY doc in the collection and deletes it.
  // TODO - this does not give me a warm fuzzy, but we need a way to clean up the test collection.
  //    The TODO peice is aspirational, in that I hope I find something better.
  firestore.collection('feedingRecords').get().then((docRefs) => {
    docRefs.forEach((val) => {
      batch.delete(val.ref);
    });
  }).then(() => {
    return batch.commit();
  }).then(() => done());;
});

test('addFeeding generates the correct action object', () => {
  const feedingRecord = feedingRecords[0];
  const action = addFeeding(feedingRecord);
  expect(action).toEqual({
    type: 'ADD_FEEDING',
    feeding: feedingRecord,
  })
});

test('addFeedingAndStore calls the DB and dispatches the action object', (done) => {
  const store = createMockStore({});

  const { wetFoodAmount, dryFoodAmount, createdAt } = feedingRecords[0];

  store.dispatch(addFeedingAndStore({ wetFoodAmount, dryFoodAmount, createdAt })).then(() => {
    const actions = store.getActions();

    expect(actions).toEqual([]);

    done();
  });
});

test('setFeedingRecords generates the correct action object', () => {
  const action = setFeedingRecords(feedingRecords);
  expect(action).toEqual({
    type: 'SET_FEEDING_RECORDS',
    feedingRecords: feedingRecords,
  })
});