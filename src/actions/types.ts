import { FeedingRecord } from '../reducers/types';

export const ADD_FEEDING_MESSAGE = 'ADD_FEEDING';
export const EDIT_FEEDING_MESSAGE = 'EDIT_FEEDING';
export const DELETE_FEEDING_MESSAGE = 'DELETE_FEEDING';
export const SET_FEEDING_RECORDS = 'SET_FEEDING_RECORDS';

interface AddFeedingRecordsAction {
  type: typeof ADD_FEEDING_MESSAGE
  feeding: FeedingRecord
}

interface EditFeedingRecordsAction {
  type: typeof EDIT_FEEDING_MESSAGE
  feeding: FeedingRecord
}

interface DeleteFeedingRecordsAction {
  type: typeof DELETE_FEEDING_MESSAGE
  feeding: FeedingRecord
}

interface SetFeedingRecordsAction {
  type: typeof SET_FEEDING_RECORDS
  feedingRecords: FeedingRecord[]
}

export type FeedingActionTypes = AddFeedingRecordsAction | EditFeedingRecordsAction | DeleteFeedingRecordsAction | SetFeedingRecordsAction