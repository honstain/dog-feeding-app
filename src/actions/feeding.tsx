import { firestore } from '../firebase';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';

import { ADD_FEEDING_MESSAGE, SET_FEEDING_RECORDS, FeedingActionTypes } from './types'
import { FeedingRecord } from '../reducers/types';

/**
 * The Redux actions for feeding
 *
 * This will contain the actions to be processed by the reducer and update the DB.
 */

export const addFeeding = (feeding: FeedingRecord): FeedingActionTypes => ({
   type: ADD_FEEDING_MESSAGE,
   feeding,
});

export const addFeedingAndStore = (feeding: FeedingRecord): ThunkAction<void, void, void, AnyAction> => {
   // TODO - not sure I have the dispatch type defined correctly
   return (dispatch: ThunkDispatch<{}, {}, AnyAction>) => {
      return firestore.collection('feedingRecords') // Todo - what should I return from this?a
      //firestore.collection('feedingRecords')
         .add(feeding)
         .then((docRef) => {
            // Disabled since whe have a live listener on the docs via Firestore.
            // dispatch(addFeeding({
            //    id: docRef.id,
            //    ...feeding,
            // }));
         });
   };
};

export const editFeedingAndStore = (id: string, feeding: FeedingRecord):
ThunkAction<void, void, void, AnyAction> => {
   return (dispatch) => {
      console.log('attempt update on', id, feeding);
      return firestore.collection('feedingRecords')
         .doc(id)
         .set(feeding)
         .then((docRef) => {
            // Disabled since whe have a live listener on the docs via Firestore.
            // dispatch(-----({
            //    id: docRef.id,
            //    ...feeding,
            // }));
         });
   };
};

export const deleteFeedingAndStore = (id: string):
ThunkAction<void, void, void, AnyAction> => {
   return (dispatch) => {
      console.log('attempt delete on', id);
      return firestore.collection('feedingRecords')
         .doc(id)
         .delete()
         .then((docRef) => {
            // Disabled since whe have a live listener on the docs via Firestore.
            // dispatch(-----({
            //    id: docRef.id,
            //    ...feeding,
            // }));
         });
   };
};

export const setFeedingRecords = (feedingRecords: FeedingRecord[]): FeedingActionTypes => ({
   type: SET_FEEDING_RECORDS,
   feedingRecords,
});
