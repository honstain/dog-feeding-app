import React from 'react';
import { FeedingListItem } from './FeedingListItem';

export const FeedingList = (props) => (
  <div>
    <div className="list-header">
      <div>Date</div>
      <div>Wet Food</div>
      <div>Dry Food</div>
    </div>
    <div className="list-body">
      {
        props.feedingRecords.map((feeding) => (
          <FeedingListItem key={feeding.id} {...feeding} />
        ))
      }
    </div>
  </div>
);
