import React from 'react';
import { connect } from 'react-redux';
import { FeedingForm } from './FeedingForm';
import { editFeedingAndStore, deleteFeedingAndStore } from '../actions/feeding';


export const EditFeedingPage = (props) => {

  const initialFeeding = {
    wetFoodAmount: props.feeding.wetFoodAmount,
    dryFoodAmount: props.feeding.dryFoodAmount,
    createdAt: props.feeding.createdAt,
  };

  const onSubmit = (feeding) => {
    const id = props.feeding.id;
    props.editFeedingAndStore(id, feeding);
    props.history.push('/');
  };

  const onRemove = () => {
    props.deleteFeedingAndStore(props.feeding.id);
    props.history.push('/');
  };

  return (
    <FeedingForm feeding={initialFeeding} onSubmit={onSubmit} onRemove={onRemove}/>
  );
};

const mapStateToProps = (state, props) => ({
  feeding: state.feedings.find((feeding) => feeding.id === props.match.params.id)
});

const mapDispatchToProps = (dispatch) => {
  return {
    editFeedingAndStore: (id, feeding) => dispatch(editFeedingAndStore(id, feeding)),
    deleteFeedingAndStore: (id) => dispatch(deleteFeedingAndStore(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditFeedingPage);