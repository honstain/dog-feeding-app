import React from 'react';
import { Link } from 'react-router-dom';
import convert from 'convert-units'
import moment from 'moment';


export const FeedingListItem = (props) => (
  <Link className="list-item" to={`/edit/${props.id}`}>
    <div>
      {moment(props.createdAt).format('MMM DD HH:mm')}
    </div>
    <div>
      {convert(props.wetFoodAmount).from('mg').to('g')}g
    </div>
    <div>
      {convert(props.dryFoodAmount).from('mg').to('g')}g
    </div>
  </Link>
);
