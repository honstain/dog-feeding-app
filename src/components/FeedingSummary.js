import React from 'react';
import convert from 'convert-units'
import moment from 'moment';

import {
  FlexibleWidthXYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
} from 'react-vis';


/**
 * It is important the records are sorted.
 *
 * @param {[{createdAt: number, wetFoodAmount: number, dryFoodAmount: number}]} feedingRecords
 * @returns {{ string: {createdAt: number, wetFoodAmount: number, dryFoodAmount: number}}}
 */
export const groupByDay = (feedingRecords) => {
  const newResult = feedingRecords.reduce((accumulator, currentValue) => {
    const day = moment(currentValue.createdAt).startOf('day').valueOf();

    if (day in accumulator) {
      accumulator[day].wetFood += currentValue.wetFoodAmount;
      accumulator[day].dryFood += currentValue.dryFoodAmount;
    } else {
      accumulator[day] = {
        day: day,
        wetFood: currentValue.wetFoodAmount,
        dryFood: currentValue.dryFoodAmount,
      };
    };
    return accumulator;

  }, {});
  return newResult;
};

/**
 * Take only the last 5 days of summary records, and fill any gaps in the data.
 *
 * @param {{ string: {createdAt: number, wetFoodAmount: number, dryFoodAmount: number}}} groupByDays
 * @returns {[{day: number, wetFood: number, dryFood: number}]}
 */
export const calculateLastFiveDays = (groupByDays) => {
  const result = [];
  const current = moment().startOf('day');

  for (let i = 0; i < 5; i++) {
    const key = current.valueOf();
    if (key in groupByDays) {
      result.push(groupByDays[key]);
    }
    else {
      result.push({ day: current.valueOf(), wetFood: 0, dryFood: 0 });
    };
    current.subtract(1, 'days');
  }
  return result;
};

export const FeedingSummary = (props) => {
  const groupByDays = groupByDay(props.feedingRecords);
  const lastFiveDays = calculateLastFiveDays(groupByDays);

  const wetFoodPlot = lastFiveDays.map((sum) =>
     ({ y: convert(sum.wetFood).from('mg').to('g'), x: moment(sum.day).format('ddd DD') })
  );
  const dryFoodPlot = lastFiveDays.map((sum) =>
     ({ y: convert(sum.dryFood).from('mg').to('g'), x: moment(sum.day).format('ddd DD') })
  );

  return (
    <div>
      <div>
        <FlexibleWidthXYPlot xType="ordinal" height={300} stackBy="y">
          <VerticalGridLines />
          <HorizontalGridLines />
          <XAxis />
          <YAxis />
          <VerticalBarSeries data={ wetFoodPlot } />
          <VerticalBarSeries data={ dryFoodPlot } />
        </FlexibleWidthXYPlot>
      </div>
    </div>
  );
};