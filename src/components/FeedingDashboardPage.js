import React from 'react';
import { connect } from 'react-redux';
import { FeedingList } from './FeedingList';
import { prepFeedingRecords } from '../selectors/feeding-records';
import { FeedingSummary } from './FeedingSummary'

export const FeedingDashboardPage = (props) => (

    <div className="content-container">
      <FeedingSummary feedingRecords={props.feedingRecords} />
      <FeedingList feedingRecords={props.feedingRecords} />
      <span></span>
    </div>

);

const mapStateToProps = (state) => {
  return {
    feedingRecords: prepFeedingRecords(state.feedings),
  };
};

export default connect(mapStateToProps)(FeedingDashboardPage);