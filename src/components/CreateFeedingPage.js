import React from 'react';
import { connect } from 'react-redux';
import { FeedingForm } from './FeedingForm';
import { addFeedingAndStore } from '../actions/feeding';
import moment from 'moment';


export const CreateFeedingPage = (props) => {

  const initialFeeding = {
    wetFoodAmount: '',
    dryFoodAmount: '',
    createdAt: moment(),
  };

  const onSubmit = (feeding) => {
    props.addFeedingAndStore(feeding);
    props.history.push('/');
  };

  return (
    <FeedingForm feeding={initialFeeding} onSubmit={onSubmit} />
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    addFeedingAndStore: (feeding) => dispatch(addFeedingAndStore(feeding)),
  };
};

export default connect(undefined, mapDispatchToProps)(CreateFeedingPage);