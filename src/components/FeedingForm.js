import React, { useState } from 'react';
import convert from 'convert-units'
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';
import TimePicker from 'rc-time-picker';


export const FeedingForm = (props) => {
  const [wetFoodAmount, setWetFood] = useState(
    props.feeding.wetFoodAmount && convert(props.feeding.wetFoodAmount).from('mg').to('g')
  );
  const [dryFoodAmount, setDryFood] = useState(
    props.feeding.dryFoodAmount && convert(props.feeding.dryFoodAmount).from('mg').to('g')
  );
  const [date, setDate] = useState(moment(props.feeding.createdAt));

  const [focused, setFocus] = useState(false);

  const onWetFoodAmountChange = (e) => {
    const value = e.target.value;
    if (!value || value.match(/^\d{1,}(\.\d{0,2})?$/)) {
      setWetFood(value);
    }
  };

  const onDryFoodAmountChange = (e) => {
    const value = e.target.value;
    if (!value || value.match(/^\d{1,}(\.\d{0,2})?$/)) {
      setDryFood(value);
    }
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const feeding = {
      wetFoodAmount: wetFoodAmount ? convert(parseFloat(wetFoodAmount, 10)).from('g').to('mg') : 0,
      dryFoodAmount: dryFoodAmount ? convert(parseFloat(dryFoodAmount, 10)).from('g').to('mg') : 0,
      createdAt: date.valueOf(),
    };
    props.onSubmit(feeding);
  };

  const onTimeChange = (value) => {
    console.log(value);
    setDate(value);
  };

  return (
    <div className="content-container">
      <form className="form" onSubmit={onSubmit}>
        <div>
          <label>Wet Food Amount</label>
          <input
            type="number"
            placeholder="Wet Food Amount in grams"
            className="text-input"
            value={wetFoodAmount}
            onChange={onWetFoodAmountChange}
          />
        </div>

        <div>
          <label>Dry Food Amount</label>
          <input
            type="number"
            placeholder="Dry Food Amount in grams"
            className="text-input"
            value={dryFoodAmount}
            onChange={onDryFoodAmountChange}
          />
        </div>

        <SingleDatePicker
          date={date}
          onDateChange={date => setDate(date)}
          focused={focused}
          onFocusChange={({ focused }) => setFocus(focused)}
          numberOfMonths={1}
          isOutsideRange={() => false}
        />

        <TimePicker
          value={date}
          defaultOpenValue={moment()}
          onChange={onTimeChange}
          showSecond={false}
          minuteStep={1}
        />

        <div className="form__buttons">
          <button className="button">Submit</button>
          { props.onRemove && (
            <button className="button button--warning" onClick={props.onRemove}>Remove</button>
          )}
        </div>
      </form>
    </div>
  );
};