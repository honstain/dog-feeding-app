import React from 'react';
import { Link } from 'react-router-dom';


const Header = () => (
  <header className="header">
    <div className="content-container">
      <div className="header__content">
        <Link className="header__title" to="/">
          <h1>Food Tracker</h1>
        </Link>

        <Link to="/create">
          <button className="button">Create</button>
        </Link>
      </div>
    </div>
  </header>
);

export default Header;