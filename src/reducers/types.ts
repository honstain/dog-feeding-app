
export interface FeedingRecord {
  id: string,
  wetFoodAmount: number,
  dryFoodAmount: number,
  createdAt: number,
}

export interface FeedingState {
  feedingRecords: FeedingRecord[],
}