import { FeedingRecord } from "./types";
import {
  FeedingActionTypes,
  ADD_FEEDING_MESSAGE,
  SET_FEEDING_RECORDS,
  EDIT_FEEDING_MESSAGE,
  DELETE_FEEDING_MESSAGE
} from "../actions/types";


const feedingReducerDefaultState: FeedingRecord[] = [];

const feedingReducer = (state = feedingReducerDefaultState, action: FeedingActionTypes) => {
  switch (action.type) {
    case ADD_FEEDING_MESSAGE:
      return [
        ...state,
        action.feeding,
      ]
    case EDIT_FEEDING_MESSAGE:
      return [
        ...state.filter((feeding) => feeding.id === action.feeding.id),
        action.feeding,
      ]
    case DELETE_FEEDING_MESSAGE:
      return state.filter((feeding) => feeding.id === action.feeding.id);
    case SET_FEEDING_RECORDS:
      return action.feedingRecords;
    default:
      return state;
  }
};

export default feedingReducer;